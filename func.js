const _ = require('lodash')

module.exports = (Redis, templates, res) => {
  let mm = {}

  mm.getDataFromRedis = url => new Promise((resolve, reject) => {
    Redis.get(url, (err, data) => {
      if (err) {
        reject(err)
      } else {
        resolve(JSON.parse(data))
      }
    })
  })

  mm.saveDataToRedis = (url, data) => new Promise((resolve, reject) => {
    Redis.set(url, JSON.stringify(data), err => {
      if (err) {
        reject(err)
      } else {
        // 1 day
        Redis.expire(url, 86400, err => {
          if (err) {
            reject(err)
          } else {
            resolve(data)
          }
        })
      }
    })
  })

  mm.removeDataFromRedis = url => {
    Redis.del(url)
  }

  mm.return403 = () => {
    res.writeHead(404)
    res.end(templates.error403())
  }

  mm.return404 = () => {
    res.writeHead(404)
    res.end(templates.error404())
  }

  mm.return200 = data => {
    if (data.indexToStart === data.agents.length) {
      data.indexToStart = 0
    }

    const agent = data.agents[data.indexToStart]

    if (_.isEmpty(agent)) {
      return mm.return403()
    }

    let number = agent.number

    if (/^\+6/.test(number.toString())) {
      number = number.replace('+', '')
    } else if (/^01/.test(number.toString())) {
      number = `6${number}`
    }

    let url = `https://wa.me/${number}`
    let url2 = `whatsapp://send?phone=+${number}`

    let msg = (_.isEmpty(_.trim(agent.msg))) ? !_.isEmpty(_.trim(data.msg)) ? data.msg : null : agent.msg
    if (!_.isEmpty(_.trim(msg))) {
      msg = msg.replace(/@agentname/g, agent.name)
      url += `?text=${encodeURI(msg)}`
      url2 += `&text=${encodeURI(msg)}`
    }

    data.whatsappLink = url
    data.whatsappLink2 = url2

    res.writeHead(200)
    res.end(templates.index(data))
    data.indexToStart++

    return data
  }

  return mm
}
