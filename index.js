require('dotenv').config()

const {PORT, DB, CORE_URL, CORE_URL_OUT_AUTH, CORE_URL_IN_AUTH} = process.env

const http = require('http')
const dot = require('dot')
const _ = require('lodash')
const axios = require('axios')
const Redis = require('redis').createClient({db: parseInt(DB)})

const templates = dot.process({ path: './template' })

http.createServer(async (req, res) => {
  const func = require('./func')(Redis, templates, res)

  const {url, method} = req
  const match = url.match(/^\/(.{6,})/)
  const matchFlush = url.match(/^\/flush\/(.{6,})/)

  // Not recognize route
  if (['GET', 'DELETE'].indexOf(method) === -1 || (method === 'GET' && _.isNull(match))) {
    return func.return403()
  }

  // Clear cache
  if (method === 'DELETE' && !_.isNull(matchFlush)) {
    const {authorization} = req.headers
    if (_.isEmpty(_.trim(authorization)) || authorization !== CORE_URL_IN_AUTH) {
      res.writeHead(403, {'Content-Type': 'application/json'})
      return res.end(JSON.stringify({success: false, msg: 'Not authorize'}))
    }
    res.writeHead(200, {'Content-Type': 'application/json'})
    func.removeDataFromRedis(matchFlush[1])
    return res.end(JSON.stringify({success: true}))
  }

  try {
    const slug = match[1]
    let data = await func.getDataFromRedis(slug)
    if (_.isEmpty(data)) {
      const response = await axios.get(`${CORE_URL}/${slug}`, {headers: {Authorization: CORE_URL_OUT_AUTH}})
      if (_.isEmpty(response.data)) {
        return func.return404()
      }
      data = response.data
      data.indexToStart = 0
    }

    data = func.return200(data)
    await func.saveDataToRedis(slug, data)
  } catch (err) {
    func.return403()
  }
}).listen(PORT, '127.0.0.1', () => console.log(`whatsapp bizsaya server is running or 127.0.0.1:${PORT}`))
